package com.embibe.dsl.clickstreamconsumer.executor

import org.apache.spark.sql.types._

object DataStruct {

  def getJsonStruct(): StructType = {
    val jsonSchemaSource = scala.io.Source.fromFile("src/main/resources/schema.json")
    val jsonSchemaString = jsonSchemaSource.getLines().mkString("\n")
    val schemaStruct = DataType.fromJson(jsonSchemaString).asInstanceOf[StructType]

    schemaStruct
  }
}
