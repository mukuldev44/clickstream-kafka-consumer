package com.embibe.dsl.clickstreamconsumer.executor

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{Column, DataFrame, SparkSession}
import org.apache.spark.sql.streaming.{OutputMode, Trigger}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

/** Clickstream Kafka Executor
  *
  * spark-shell --master yarn --driver-memory 2G --executor-memory 5G --num-executors 5 --executor-cores 5 --packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0,org.elasticsearch:elasticsearch-hadoop:6.3.1,com.typesafe:config:1.3.2,com.amazonaws:aws-java-sdk-s3:1.11.438,eu.bitwalker:UserAgentUtils:1.21,org.postgresql:postgresql:42.2.5,mysql:mysql-connector-java:8.0.13 --name dev_stream_shell
  *
  * Developed By Mukul Dev on 25th Feb, 2019
  */
object ClickstreamExecutor {

  def main(args: Array[String]): Unit = {
    // SparkSession
    val spark = SparkSession.builder
      .master("yarn")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()

    // Root Logger Level
    val rootLogger = Logger.getRootLogger
    rootLogger.setLevel(Level.ERROR)

    val inputDf = spark
      .readStream // Stream Reader
      .format("kafka")
      .option("kafka.bootstrap.servers", "10.140.10.108:9092, 10.140.10.103:9092, 10.140.10.11:9092")
      .option("subscribe", "segmentio-logs-sink-rewrite-prod")
      .load()

    val valueJsonStringDF = inputDf.select(col("value").cast(StringType))

    val schemaStruct = getDataStruct()
    val valueJsonDF = valueJsonStringDF.select(from_json(col("value"), schemaStruct).as("json"))
    val nestedValueDF = valueJsonDF.select("json.*")

    // Flattening the nested schema
    val flattenedColumns = flattenSchema(schemaStruct)
    val flattenedRenamedCols = flattenedColumns.map{ name =>
      col(name.toString())
        .as(name.toString()
          .replace(".","-") // Renaming columns, concatenating parent and child with `-`
          .replace(" ", "_")) // Parquet does not allows space in column names
    }
    val flattenedVaueDF = nestedValueDF.select(flattenedRenamedCols: _*)

    // Transformations
    val dateTransformedDF = getDateTransformedDF(flattenedVaueDF)

    // Output Sink
    val consoleOutput = dateTransformedDF
      .writeStream
      .partitionBy("year", "month", "month_day")
      .outputMode(OutputMode.Append())
      .trigger(Trigger.ProcessingTime("2 minutes"))
      .format("parquet")
      .option("path", "s3://dsl-datastore/data/clickstream-kafka-consumer/logs/")
      .option("checkpointLocation", "s3://dsl-datastore/data/clickstream-kafka-consumer/spark-checkpoint/rewrite-prod/")
      .start()

    consoleOutput.awaitTermination()
  }

  /** Creating schema struct
    *
    * @return
    */
  def getDataStruct(): StructType = {
    val jsonSchemaSource = scala.io.Source.fromFile("src/main/resources/schema.json")
//    val jsonSchemaSource = scala.io.Source.fromFile("/home/hadoop/schema.json")
    val jsonSchemaString = jsonSchemaSource.getLines().mkString("\n")
    val schemaStruct = DataType.fromJson(jsonSchemaString).asInstanceOf[StructType]

    schemaStruct
  }

  def flattenSchema(schema: StructType, prefix: String = null) : Array[Column] = {
    schema.fields.flatMap(f => {
      val colName = if (prefix == null) f.name else (prefix + "." + f.name)

      f.dataType match {
        case st: StructType => flattenSchema(st, colName)
        case _ => Array(col(colName))
      }
    })
  }

  def getDateTransformedDF(df: DataFrame): DataFrame = {
    val dateDF = df
      .withColumn("date", to_date(col("timestamp")))
      .withColumn("year", year(col("date")))
      .withColumn("month", month(col("date")))
      .withColumn("week", weekofyear(col("date")))
      .withColumn("month_day", dayofmonth(col("date")))
      .withColumn("hour", hour(to_timestamp(col("timestamp"))))

    dateDF
  }
}
